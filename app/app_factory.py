import json
from flask import Flask, current_app, render_template, session, url_for, redirect, flash
from werkzeug.contrib.fixers import ProxyFix

from app.authentication.cern_oauth import load_cern_oauth
from app.cli import check_eos_quota_web


def init_oauth(app):
    print("Initializing Oauth... ", end='')
    load_cern_oauth(app)
    print("ok")


def create_app(config_filename):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)

    if app.config.get('USE_PROXY', False):
        app.wsgi_app = ProxyFix(app.wsgi_app)

    init_oauth(app)

    @app.route("/")
    def index():
        current_user = session.get('user', None)
        with open(current_app.config['DATA_FILE_NAME']) as data_file:
            try:
                data = json.load(data_file)
            except ValueError:
                data = {}


        eos_quota_check = check_eos_quota_web()

        return render_template('index.html', data=data, current_user=current_user, eos_check=eos_quota_check)

    @app.route('/logout')
    def logout():
        """
        View to log out a user on the site
        :return: Redirects to the users.index after logout
        """
        session["user"] = None
        flash('You have logged out')
        return redirect(url_for('index'))

    return app
