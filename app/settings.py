from app.secret import config


class BaseConfig(object):
    APP_PORT = config.APP_PORT
    SECRET_KEY = config.SECRET_KEY

    """
    Debug and logging configuration
    """
    DEBUG = config.DEBUG
    TESTING = config.TESTING

    """
    Whether or not to use the wsgi Proxy Fix
    """
    USE_PROXY = config.USE_PROXY

    DATA_FILE_NAME = config.DATA_FILE_NAME
    EMAIL_RECIPIENT = config.EMAIL_RECIPIENT
    SEND_EMAIL = config.SEND_EMAIL
    ERROR = config.ERROR
    ERROR_FILE_NAME = config.ERROR_FILE_NAME

    # Oauth
    CERN_OAUTH_CLIENT_ID = config.CERN_OAUTH_CLIENT_ID
    CERN_OAUTH_CLIENT_SECRET = config.CERN_OAUTH_CLIENT_SECRET
    OAUTH_ADMIN_EGROUP = config.OAUTH_ADMIN_EGROUP

    LOG_LEVEL = config.LOG_LEVEL
    WEBAPP_LOGS = config.WEBAPP_LOGS
    SCRIPT_LOGS = config.SCRIPT_LOGS

    MAIL_HOSTNAME = config.MAIL_HOSTNAME
    MAIL_FROM = config.MAIL_FROM
    MAIL_TO = config.MAIL_TO

    EOS_TOTAL_QUOTA = config.EOS_TOTAL_QUOTA
    EOS_MAX_QUOTA = config.EOS_MAX_QUOTA
    EOS_FOLDER = config.EOS_FOLDER

    HOSTNAMES = config.HOSTNAMES
