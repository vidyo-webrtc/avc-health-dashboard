from app.app_factory import create_app
from app.cli import initialize_cli
from app.settings import BaseConfig
from app.logger import setup_webapp_logs

app = create_app(BaseConfig)

setup_webapp_logs(app)
initialize_cli(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, ssl_context=('/opt/app-root/ssl/cert.pem', '/opt/app-root/ssl/key.pem'))
