import datetime
import time
import json
import os
import logging
import socket
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import requests
import schedule
from dateutil import tz
from flask import current_app
from requests.packages.urllib3.exceptions import InsecureRequestWarning, InsecurePlatformWarning, SNIMissingWarning
import smtplib

logger = logging.getLogger('webapp.script')


def check_eos_quota():
    bash_command = ["ls", "-l", "-d", "-h", "--block-size=G", current_app.config["EOS_FOLDER"]]
    import subprocess
    result = subprocess.check_output(bash_command)
    result = result.decode(encoding='UTF-8')

    return result


def build_eos_quota_email(current_date, used_quota_in_gb):
    from_domain = socket.getfqdn()
    logger.debug(current_app.config["EOS_TOTAL_QUOTA"])

    total_quota = str(current_app.config["EOS_TOTAL_QUOTA"])

    html = """\
            <html>
              <head></head>
              <body>
                <p>Hello, </p>
                <p>The quota of EOS has reached it's threshold of <strong>""" + str(current_app.config[
                                                                                                       "EOS_MAX_QUOTA"]) + """ GB</strong> on """ + current_date + """ reported by """ + from_domain + """:</p>
                <ul>
                    <li><strong>Quota used: </strong> """ + used_quota_in_gb + """ GB</li>
                    <li><strong>Total Quota: </strong> """ + total_quota + """ GB</li>
                </ul>
                <p>Remember the <strong>Total Quota and the threshold must be set manually</strong>.</p>
                <p>You have more information about this on the AVC Docs: https://dev-documentation.web.cern.ch/opsdoc/ </p>
                <p>Best regards.</p>
                <p>The automated script.</p>
                <p><a href="https://avc-status.web.cern.ch/" title="AVC Status website">https://avc-status.web.cern.ch/</a></p>
               </body>
            </html>
            """

    return html


def check_eos_quota_web():
    result = check_eos_quota()

    if result:
        result_pieces = result.split()

        if len(result_pieces) > 4:
            logger.info(result_pieces[4])
            used_quota_in_gb = result_pieces[4][:-1]
            logger.debug(current_app.config["EOS_MAX_QUOTA"])
            result_to_return = {'used_quota': int(used_quota_in_gb),
                                'total_quota': current_app.config["EOS_MAX_QUOTA"]}
            if int(used_quota_in_gb) > current_app.config["EOS_MAX_QUOTA"]:
                result_to_return['status'] = 'error'
                return result_to_return
            else:
                result_to_return['status'] = 'ok'
                return result_to_return


def send_email(html, subject):
    """

    :param html: 
    :param subject: 
    :return: 
    """
    from_domain = socket.getfqdn()
    from_email = current_app.config['MAIL_FROM']

    logger.debug(html)
    msg = MIMEMultipart()
    msg.attach(MIMEText(html, 'html'))
    msg['Subject'] = subject
    msg['From'] = from_email
    msg['To'] = current_app.config['EMAIL_RECIPIENT']
    logger.info("About to send an Email to {}".format(current_app.config['EMAIL_RECIPIENT']))
    s = smtplib.SMTP(current_app.config['MAIL_HOSTNAME'])
    s.sendmail(from_email, current_app.config['EMAIL_RECIPIENT'], msg.as_string())
    logger.info("Email sent to {}".format(current_app.config['EMAIL_RECIPIENT']))
    s.quit()


def check_eos_quota_email():
    result = check_eos_quota()

    if result:
        result_pieces = result.split()

        if len(result_pieces) > 4:
            logger.info(result_pieces[4])
            used_quota_in_gb = result_pieces[4][:-1]
            logger.debug(current_app.config["EOS_MAX_QUOTA"])
            if int(used_quota_in_gb) > current_app.config["EOS_MAX_QUOTA"]:
                logger.info("EOS Threshold reached")
                to_zone = tz.gettz('Europe/Zurich')
                current_date = datetime.datetime.now(tz=to_zone).strftime('%Y-%m-%d %H:%M')
                html = build_eos_quota_email(current_date, used_quota_in_gb)
                send_email(html, '[AVC Status] EOS Quota threshold reached ' + current_date)


def initialize_cli(app):
    """
    Add additional commands to the CLI. These are loaded automatically on the main.py

    :param app: App to attach the cli commands to
    :return: None
    """

    def send_emails():

        to_zone = tz.gettz('Europe/Zurich')
        current_date = datetime.datetime.now(tz=to_zone).strftime('%Y-%m-%d %H:%M')
        data = {}
        with open(current_app.config['DATA_FILE_NAME'], 'r') as file:
            try:
                data = json.load(file)
            except ValueError as e:
                print("Data not found: {}".format(str(e)))
        results = []
        for server in data:
            if server['error_count'] == 3:
                results.append(server)

        if len(results) > 0 and current_app.config['SEND_EMAIL']:
            html = build_ping_results_email(current_date, results)
            send_email(html, '[AVC Status] Some machines not responding ' + current_date)

    def ping_servers(current_date, data):
        results = []
        for hostname in current_app.config['HOSTNAMES']:
            new_result = {'name': hostname[0], 'hostname': hostname[1], 'type': hostname[3]}
            response = os.system("ping -c 1 " + hostname[1] + " > /dev/null")
            if response == 0:
                make_request(hostname, new_result)
            else:
                new_result['status'] = 'not ok'
                new_result['details'] = "not reachable"

            if new_result['status'] == 'not ok':
                found = False
                for server in data:
                    if server['hostname'] == hostname[1]:
                        try:
                            new_result['error_count'] = server['error_count'] + 1
                            new_result['first_error_date'] = server['first_error_date']
                            found = True
                        except KeyError:
                            script_logger.warning("Error count not found")
                            new_result['error_count'] = 1
                            new_result['first_error_date'] = current_date
                            found = True
                        break
                if not found:
                    new_result['error_count'] = 1
                    new_result['first_error_date'] = current_date

                new_result['last_error_date'] = current_date
            else:
                new_result['error_count'] = 0

            new_result['timestamp'] = current_date
            new_result['path'] = hostname[2]
            results.append(new_result)

        return results

    def make_request(hostname, new_result):
        """
        
        :param hostname: 
        :param new_result: 
        :return: 
        """
        try:
            r = requests.get('https://' + hostname[1] + hostname[2], verify=False)
            if r.status_code != 200:
                new_result['status'] = "not ok"
                new_result['details'] = "ERROR: Response status code is: " + str(r.status_code)
            else:
                new_result['status'] = "ok"
        except requests.exceptions.RequestException as e:
            new_result['status'] = 'not ok'
            new_result['details'] = "EXCEPTION: " + str(e)

    def build_ping_results_email(current_date, results):
        from_domain = socket.getfqdn()
        results_in_rows = ""
        for result in results:
            results_in_rows += "<tr><td>" + result['name'] + "</td><td>" + result['hostname'] + "</td><td>" + \
                               result['details'] + "</td><td>" + result['timestamp'] + "</td></tr>"

        html = """\
                <html>
                  <head></head>
                  <body>
                    <p>Hello, </p>
                    <p>The following websites are not reachable """ + current_date + """ reported by instance """ + from_domain + """:</p>
                    <table>
                        <thead>
                            <tr><th>Server Name</th><th>Hostname</th><th>Error</th><th>Date & time</th></tr>
                        </thead>
                        <tbody>
                        """ + results_in_rows + """
                        </tbody>
                    </table>
                    <p>Best regards.</p>
                    <p>The automated script.</p>
                   </body>
                </html>
                """

        return html

    def update_json():
        logger.info("Script is running: Updating JSON")
        logger.info("JSON location is: {}".format(current_app.config['DATA_FILE_NAME']))
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)
        requests.packages.urllib3.disable_warnings(SNIMissingWarning)

        to_zone = tz.gettz('Europe/Zurich')
        current_date = datetime.datetime.now(tz=to_zone).strftime('%Y-%m-%d %H:%M')

        original_data = {}
        with open(current_app.config['DATA_FILE_NAME'], 'r') as file:
            try:
                original_data = json.load(file)
            except ValueError as e:
                print("Data not found: {}".format(str(e)))

        results = ping_servers(current_date, original_data)

        with open(current_app.config['DATA_FILE_NAME'], 'w') as f:
            json.dump(results, f, indent=2, sort_keys=True, ensure_ascii=False)

        send_emails()

    @app.cli.command()
    def send_test_email():
        """

        :return: 
        """

        html = """\
                <html>
                  <head></head>
                  <body>
                    <p>Hello, </p>
                    <p>This is a test email. Please ignore it.</p>
                    <p>Best regards.</p>
                    <p>The automated script.</p>
                   </body>
                </html>
                """

        send_email(html, '[AVC Status] AVC Health Test')

    @app.cli.command()
    def update_json_forever():

        schedule.every(120).seconds.do(update_json)
        schedule.every(4).weeks.do(check_eos_quota_email)

        while True:
            schedule.run_pending()
            time.sleep(20)

    @app.cli.command()
    def check_eos():
        check_eos_quota_email()
