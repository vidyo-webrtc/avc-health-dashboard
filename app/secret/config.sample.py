APP_PORT = 8080
SECRET_KEY = '\\Zq?\xd8\x1b@Xi\xdc\xbe]\xd7\xe0\xef\xb4\xbd1\xd0G}4\x8dT'

"""
Debug and logging configuration
"""
DEBUG = True
TESTING = False

"""
Whether or not to use the wsgi Proxy Fix
"""
USE_PROXY = True

DATA_FILE_NAME = "/opt/app-root/files/data.json"
EMAIL_RECIPIENT = "rene.fernandez@cern.ch"
SEND_EMAIL = True
ERROR = False
ERROR_FILE_NAME = "/opt/app-root/files/error.json"

# Oauth
CERN_OAUTH_CLIENT_ID = ""
CERN_OAUTH_CLIENT_SECRET = ""
OAUTH_ADMIN_EGROUP = "webcast-team"

LOG_LEVEL = "DEV"
WEBAPP_LOGS = "/opt/app-root/webapp.log"
SCRIPT_LOGS = "/opt/app-root/script.log"

MAIL_HOSTNAME = "cernmx.cern.ch"
MAIL_FROM = "website@webcast.web.cern.ch"
MAIL_TO = "rene.fernandez@cern.ch"

EOS_TOTAL_QUOTA = 4500
EOS_MAX_QUOTA = 3600
EOS_FOLDER = "/tmp"


HOSTNAMES = (
    ["CERN Vidyo WebRTC Client", "vidyowebrtc.web.cern.ch", "/", "official"],
    ["CERN Vidyo WebRTC Client QA", "vidyowebrtctest.web.cern.ch", "/", "test"],
    ["EOS Samba Gateway", "wowza.cern.ch", "/AtlasLive/smil:stream3.smil/playlist.m3u8", "official"],
    # RAVEM
    ["RAVEM", "ravem.web.cern.ch", "/apiv2/hello", "official"],
    ["RAVEM QA", "ravemqa.web.cern.ch", "/apiv2/hello", "test"],
    # Webcast Stats
    ["Webcast Stats", "webcast-stats.web.cern.ch", "", "official"],
    ["Webcast Stats Test", "test-webcast-stats.web.cern.ch", "", "test"],
    # WEBCAST WEBSITE
    ["Webcast Website", "webcast.web.cern.ch", "/api/v1/hello/", "official"],
    ["Webcast Website Test", "webcast-test.web.cern.ch", "/api/v1/hello/", "test"],
    # VIDYO WEBRTC
    ["WebRTC Backend", "vidyowebrtc-backend.web.cern.ch", "/api/v1.0/hello/", "official"],
    ["WebRTC Backend QA", "vidyowebrtc-backend-qa.web.cern.ch", "/api/v1.0/hello/", "test"],

)