from datetime import datetime
from logging.handlers import SMTPHandler
import logging
from pytz import timezone, utc

import sys


def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def setup_webapp_logs(app, to_stdout=True):
    """
    Configures the application logging
    
    :param app: Flask application where the logging will be configured
    :param to_file: Whether to write the log to a file or not
    :return: 
    """
    logger = logging.getLogger('webapp')

    if app.config['LOG_LEVEL'] == 'DEV':
        logger.setLevel(logging.DEBUG)

    if app.config['LOG_LEVEL'] == 'PROD':
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(levelname)s - %(asctime)s - %(name)s - %(message)s - %(pathname)s - %(funcName)s():%(lineno)d')

    logging.Formatter.converter = zurich_time

    if to_stdout:
        print("Logging to stdout -> True")
        configure_stdout_logging(logger=logger, formatter=formatter, log_level=app.config['LOG_LEVEL'])

    if app.config.get('SEND_EMAIL'):
        configure_email_logging(logger=logger, app=app)


def configure_stdout_logging(logger=None, formatter=None, log_level="DEV"):
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == 'DEV':
        stream_handler.setLevel(logging.DEBUG)
    if log_level == 'PROD':
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)


def configure_email_logging(logger=None, app=None):
    fmt_email = logging.Formatter("""
            Message type:  %(levelname)s
            Name:          %(name)s
            Location:      %(pathname)s:%(lineno)d
            Module:        %(module)s/%(filename)s
            Function:      %(funcName)s
            Time:          %(asctime)s
            Message:

            %(message)s
        """)

    # email in case of errors
    mail_handler = SMTPHandler(app.config.get('MAIL_HOSTNAME'),
                               app.config.get('MAIL_FROM'),
                               app.config.get('MAIL_TO'), "[AVC Status] Application error")
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(fmt_email)
    logger.addHandler(mail_handler)
