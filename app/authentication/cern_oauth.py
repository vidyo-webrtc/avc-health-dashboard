import jwt
import logging
from flask import flash, current_app, session
from flask_dance import OAuth2ConsumerBlueprint
from flask_dance.consumer import oauth_authorized


logger = logging.getLogger('webapp.oauth')

def load_cern_oauth(app):
    """
    Loads the CERN Oauth into the application
    
    :param app: Flask application where the CERN Oauth will be loaded
    :return: 
    """
    oauth = OAuth2ConsumerBlueprint(
        'cern_oauth',
        __name__,
        url_prefix='/oauth',
        # oauth specific settings
        token_url='https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token',
        authorization_url='https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth',
        # local urls
        login_url='/cern',
        authorized_url='/cern/authorized',
        client_id=app.config.get('CERN_OAUTH_CLIENT_ID', ''),
        client_secret=app.config.get('CERN_OAUTH_CLIENT_SECRET', '')
    )

    app.register_blueprint(oauth)

    @oauth_authorized.connect_via(oauth)
    def cern_logged_in(bp, token):
        # it anymore after getting the data here.
        response = oauth.session.get('https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo')
        payload = jwt.decode(token["access_token"], verify=False)

        is_admin = False
        if current_app.config.get('OAUTH_ADMIN_EGROUP') in \
                payload["resource_access"][app.config.get('CERN_OAUTH_CLIENT_ID', '')]["roles"]:
            is_admin = True

        response.raise_for_status()
        logger.info('User {} roles are {}'.format(payload['cern_upn'].strip(), str(
            payload["resource_access"][app.config.get('CERN_OAUTH_CLIENT_ID', '')]["roles"])))
        flash("You are {name}".format(name=payload['name'].strip()))

        session['user'] = {'username': payload['cern_upn'].strip(),
                           'person_id': payload['cern_person_id'],
                           'email': payload['email'].strip(),
                           'first_name': payload['given_name'].strip(),
                           'last_name': payload['family_name'].strip(),
                           'is_admin': is_admin}

        logger.info(
            'OAuth login successful for {} ({} {}) is_admin={}'.format(payload['cern_upn'], payload['email'],
                                                                       payload['cern_person_id'], is_admin))
