from app.cli import initialize_cli
from app.settings import BaseConfig
from app.app_factory import create_app
from app.logger import setup_webapp_logs

application = create_app(BaseConfig)

setup_webapp_logs(application)
initialize_cli(application)
