FROM centos/python-36-centos7

EXPOSE 8080

# Install yum packages with the ROOT user
USER root

ENV LD_LIBRARY_PATH /opt/rh/rh-python36/root/usr/lib64:/opt/rh/httpd24/root/usr/lib64
ENV PYTHONPATH /opt/app-root/src
ENV FLASK_APP /opt/app-root/src/wsgi.py

# Set the permissions for the app-user user
RUN chgrp -R 0 /opt/app-root && chmod -R ug+rwx /opt/app-root

USER 1001

WORKDIR /opt/app-root/src

RUN pip install --upgrade pip

# Install pip requirements
COPY requirements.txt /opt/app-root/src/requirements.txt
RUN pip install -r requirements.txt

CMD ["python", "wsgi.py"]
